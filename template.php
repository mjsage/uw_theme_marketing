<?php

/**
 * @file
 * Themes field collection items printed using the field_collection_view formatter.
 */

/**
 *
 */
function uw_theme_marketing_css_alter(&$css) {
  // We only loaded the core theme for its functions, we don't want its CSS.
  $path = drupal_get_path('theme', 'uw_fdsu_theme_resp') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($path)) == $path) {
      unset($css[$key]);
    }
  }
  $path_ctools = drupal_get_path('module', 'ctools') . '/css/';
  $path_global_footer = drupal_get_path('module', 'uw_nav_global_footer') . '/css/';
  $path_site_footer = drupal_get_path('module', 'uw_nav_site_footer') . '/css/';
  $path_system_theme = drupal_get_path('module', 'system') . '/';
  $path_cta = drupal_get_path('module', 'uw_ct_embedded_call_to_action') . '/css/';
  $path_site_share = drupal_get_path('module', 'uw_social_media_sharing') . '/css/';
  $path_ff = drupal_get_path('module', 'uw_ct_embedded_facts_and_figures') . '/css/';
  $path_rmc = drupal_get_path('module', 'responsive_menu_combined') . '/css/';

  $exclude = array(
    $path_ctools . 'modal.css' => TRUE,
    $path_global_footer . 'uw_nav_global_footer.css' => TRUE,
    $path_cta . 'uw_ct_embedded_call_to_action.css' => TRUE,
    $path_system_theme . 'system.base.css' => TRUE,
    $path_system_theme . 'system.theme.css' => TRUE,
    $path_site_footer . 'uw_nav_site_footer.css' => TRUE,
    $path_site_share . 'uw_social_media_sharing.css' => TRUE,
    $path_ff . 'highlighted_fact.css' => TRUE,
    $path_rmc . 'responsive_menu_combined.css' => TRUE,
  );
  $css = array_diff_key($css, $exclude);
}

/**
 *
 */
function uw_theme_marketing_preprocess_html(&$variables) {
  // Add meta tags to enable mobile view.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ),
  );
  $meta_mobile_view = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0',
    ),
  );
  $utm_path = drupal_get_path('theme', 'uw_theme_marketing') . '/css/utm.css';
  drupal_add_css($utm_path);
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
}

function uw_theme_marketing_preprocess_entity(&$variables) {

    $entity = $variables['elements']['#entity'];
    $entity_type = $variables['elements']['#entity_type'];
    $variables['entity_id'] = entity_id($entity_type, $entity);

}

function uw_theme_marketing_js_alter(&$javascript) {
  global $base_path;
  global $base_url;
  if (module_exists('uw_header_search')) {
    $utm_path = drupal_get_path('module', 'uw_header_search') . '/uw_header_search.js';
    drupal_add_js($utm_path);
    // drupal_add_js($tabsjs_path);
    drupal_add_js(array(
      'CToolsModal' => array(
        'modalSize' => array(
          'type' => 'scale',
          'width' => 1,
          'height' => 1,
        ),
        'modalOptions' => array(
          'opacity' => .98,
          'background-color' => '#252525',
        ),
        'animation' => 'fadeIn',
        'animationSpeed' => 'slow',
        'throbberTheme' => 'CToolsThrobber',
      // Customize the AJAX throbber like so:
      // This function assumes the images are inside the module directory's "images"
      // directory:
        'throbber' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t('Loading...'),
          'title' => t('Loading'),
        )
        ),
        'closeText' => '',
        'closeImage' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t(''),
          'title' => t(''),
        )
        ),
      ),
    ), 'setting');
    $exclude = array(
    // 'profiles/uw_base_profile/modules/features/uw_ct_home_page_banner/js/uw_banner_random.js' => FALSE,
    // 'profiles/uw_base_profile/modules/features/uw_ct_home_page_banner/js/uw_banner_slideshow.js' => FALSE,.
  );

    $javascript = array_diff_key($javascript, $exclude);
  }
}
