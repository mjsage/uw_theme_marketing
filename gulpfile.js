/**
 * @file
 */

module.exports = (function () {

'use strict';

var pkg = require('./package.json'),
      gulp = require('gulp'),
      glob = require('glob'),
      gutil = require('gulp-util'),
      minifyCSS = require('gulp-minify-css'),
      pngquant = require('imagemin-pngquant'),
      plugins = require('gulp-load-plugins')(),
      browserSync = require('browser-sync').create();

var config = {
    sass:'./sass/**/*.{scss,sass}',
    sassSrc:'./sass/utm.scss',
    sassIe:'./sass/ie.scss',
    css:'./css',
    js:'./scripts',
    jsSrc:[
       // './js/facts-figures.js',
       // './js/global.js',
       // './js/jquery.js',.
       './js/utm.js'
       ],
    images:'./images/*.{png,gif,jpeg,jpg,svg}',
    imagesmin:'./images/minified',
};
var AUTOPREFIXER_BROWSERS = [
    '> 1%',
    'ie >= 8',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
];

var onError = function (err) {
    gutil.log(err);
    this.emit('end');
};

gulp.task('styles',function () {
    return gulp.src(config.sassSrc)
       // .pipe(plugins.sourcemaps.init())
    .pipe(plugins.plumber())
    .pipe(plugins.sass({
      includePaths: require('node-bourbon').includePaths,
      outputStyle: 'collapsed'
    }))
    .on('error', function (err) {
        gutil.log(err);
        gutil.beep();
        this.emit('end');
       })

    .pipe(plugins.autoprefixer({
      browsers:AUTOPREFIXER_BROWSERS,
      cascade: false
    }))
    .pipe(minifyCSS())
    .pipe(plugins.concat('utm.css'))
    .pipe(gulp.dest(config.css))
    .pipe(browserSync.stream())
    .pipe(plugins.size({title:'css'}));
});

// gulp.task('scripts',function() {
//     return gulp.src(config.jsSrc)
//         .on('error', function (err) {
//             gutil.log(err);
//             this.emit('end');
//             growlNotifier({
//             title: 'js',
//             message: 'error'
//           });
//         })
//         .pipe(plugins.jshint())
//         .pipe(plugins.jshint.reporter('jshint-stylish'))
//           // .pipe(plugins.jshint.reporter('fail'));
//         .pipe(plugins.uglify())
//         // .pipe(plugins.rename({suffix:".min"}))
//         .pipe(gulp.dest(config.js));
// });.
// compile js files.
  gulp.task('compile:js',function () {
     return  gulp.src(config.jsSrc)
        .pipe(plugins.uglify())
        .pipe(plugins.concat('utm.min.js'))
        .pipe(gulp.dest(config.js))
        // .pipe(plugins.livereload());
  });
// Lint js files.
  gulp.task('lint',function () {
    return gulp.src('./js/utm.js')
      .pipe(plugins.jshint())
      .pipe(plugins.jshint.reporter('jshint-stylish'));
        // .pipe(plugins.jshint.reporter('fail'));.
  });

gulp.task('images',function () {
    return gulp.src(config.images)
        // .pipe(plugins.plumber({
        //     errorHandler: onError
        // }))
        // .pipe(plugins.imagemin({
        //     optimizationLevel:7,
        //     progressive:true,
        //     interlaced:true,
        // }))
        .pipe(plugins.imagemin({
                optimizationLevel:5,
                progressive: true,
                svgoPlugins: [{removeViewBox: false}],
                use: [pngquant()]
         }))
        .pipe(gulp.dest(config.imagesmin))
        .pipe(plugins.size({title:'images'}));
});
gulp.task('drush', plugins.shell.task([
    'drush cache-clear theme-registry'
]));
gulp.task('browser-sync',['styles'], function () {
    browserSync.init({
        // Change to localhost you are using.
        proxy: "https://gis2"
    });
    gulp.watch(config.jsSrc,['compile:js']).on('change', browserSync.reload);
    gulp.watch(config.sass,['styles']).on('change', browserSync.reload);
    gulp.watch(config.images,['images']);
    gulp.watch(config.css).on('change', browserSync.reload);
});
gulp.task('default',[],function () {
    gulp.start('styles','compile:js','images','watch');
});
gulp.task('browser',[],function () {
    gulp.start('styles','compile:js','images','browser-sync');
});
gulp.task('watch',[],function () {

    gulp.watch(config.jsSrc,['lint']);
    gulp.watch(config.sass,['styles']);
    gulp.watch(config.images,['images']);

    // gulp.watch('**/*.{php,inc,info}',['drush']);.
});
})();
