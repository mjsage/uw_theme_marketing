<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

hide($content['field_sph_background_image']);

// In case we need to use an image style, here's the code from a different theme that would accomplish this:
// image_style_url('theme_icon__landing_page_', $content['field_theme_area']['#items'][0]['taxonomy_term']->field_theme_page_icon[LANGUAGE_NONE][0]['uri'])
?>

<div class="marketing-item-wrap" style="background-image:url('<?php print file_create_url($content['field_sph_background_image']['#items'][0]['uri']); ?>');">
  <div class="marketing-item">
      <div id="paragraph-<?php print $entity_id; ?>"></div>
    <?php print render($content); ?>
  </div>
</div>
