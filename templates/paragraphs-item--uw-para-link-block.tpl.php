<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
$classes = array();
$imageinfo = '';
$classes[] = 'pathway';
if (!empty($content['field_sph_background_image'])) {
  hide($content['field_sph_background_image']);
  $classes[] = 'has_image';
  $imageinfo = ' style="background-image:url(\'' . file_create_url($content['field_sph_background_image']['#items'][0]['uri']) . '\');"';
}
if (!empty($content['field_sph_colouring'])) {
  hide($content['field_sph_colouring']);
  $classes[] = $content['field_sph_colouring']['#items'][0]['value'];
}
$classes = drupal_attributes(array('class' => $classes));
?>
<div <?php print $classes; ?> <?php print $imageinfo; ?>>
    <div id="paragraph-<?php print $entity_id; ?>"></div>
  <a href="<?php print $content['field_sph_link']['#items'][0]['url']; ?>">
    <span class="pathway-flex">
    <h3><?php print check_plain($content['field_sph_link']['#items'][0]['title']); ?></h3>
    <p><?php print render($content['field_sph_link_description']); ?></p>
    <?php
    // Hide content we've already rendered in a non-standard way (doing it the standard way would hide it automatically)
    hide($content['field_sph_link']);
    // We've rendered everything that existed when this theme was created, but render any potential new items so people don't think they're lost.
    print render($content);
    ?>
    </span>
  </a>
</div>
