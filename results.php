<?php

/**
 * @file
 */

// Change based on current environment.
$CNAME = 'pole.uwaterloo.ca';
$rr = getenv("HTTP_REFERER");
$qq = "https://$CNAME/urp";

$a = parse_url($rr, PHP_URL_HOST);
$bb = parse_url($qq, PHP_URL_HOST);
$b = str_replace('_', '', $bb);

if (trim($a) == trim($b) || $rr == '' || trim($a) == 'cas.uwaterloo.ca') {
  require_once 'db_settings.php';

  if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header("Location: index.php");
  }
  $grad = 0;
  if (isset($_POST['grad']) && $_POST['grad'] == 1) {
    $grad = 1;
  }

  $undergrad = 0;
  if (isset($_POST['undergrad']) && $_POST['undergrad'] == 1) {
    $undergrad = 1;
  }

  $keyword = '';
  if (isset($_POST['program-keyword'])) {
    $keyword = substr(trim(htmlentities($_POST['program-keyword'])), 0, 100);
  }

  // Checks if the query yields any parttime or online programs.
  $is_pt_or_online = FALSE;
  if ($undergrad || $undergrad == $grad) {
    // $STMT = $link->prepare($uquery);
    $STMT = $link->prepare("CALL `urp`.`ADMISSIONS_PROGRAM_SEARCH`(1,?);");
    $STMT->bind_param('s', $keyword);
    // Execute query.
    if (!$STMT->execute()) {
      exit("ERROR: $STMT->error");
    }

    $STMT->bind_result($program_name, $program_url, $part_time, $online, $entry_type);

    $undergrad_programs = new ArrayObject();

    while (mysqli_stmt_fetch($STMT)) {
      $undergrad_programs->append(array('name' => $program_name, 'url' => $program_url, 'part_time' => $part_time, 'online' => $online));
      if ($part_time||$online) {
        $is_pt_or_online = TRUE;
      }
    }
    $STMT->close();
    $link->next_result();
  }
  if ($grad || $undergrad == $grad) {
    // $STMT = $link->prepare($gquery);
    $STMT = $link->prepare("CALL `urp`.`ADMISSIONS_PROGRAM_SEARCH`(2,?);");
    $STMT->bind_param('s', $keyword);
    // Execute query.
    if (!$STMT->execute()) {
      exit("ERROR: $STMT->error");
    }

    $STMT->bind_result($program_name, $program_url, $part_time, $online, $entry_type);

    $grad_programs = new ArrayObject();

    while (mysqli_stmt_fetch($STMT)) {
      $grad_programs->append(array('name' => $program_name, 'url' => $program_url, 'part_time' => $part_time, 'online' => $online));
      if ($part_time||$online) {
        $is_pt_or_online = TRUE;
      }
    }
    $STMT->close();
    $link->next_result();
  }
}
else {
  echo $a . "<br>" . $b;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Admissions | University of Waterloo</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/admissions.css"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/global.js"></script>
</head>

<body>
  <div id="header">
      <div id="header-content">
            <div id="header-logo"><img src="images/uwaterloo-logo.png" width="206" height="49" alt="University of Waterloo Logo" /></div>
            <div id="toggle-search-label"></div>
            <input type="checkbox" id="toggle-search-checkbox" name="toggle-search">
            <div id="toggle-menu-label"></div>
            <input type="checkbox" id="toggle-menu-checkbox" name="toggle-search">
            <div id="global-search">
              <form method="get" action="//uwaterloo.ca/search">
                    <div>
                        <label id="uw-search-label" for="uw-search-term" class="js" style="left: 60.3125px;">Search</label>
                        <input id="uw-search-term" placeholder="SEARCH" type="text" size="31" tabindex="2" accesskey="4" name="q">
                        <input id="uw-search-submit" class="chevron-submit" type="submit" value="Search" tabindex="3">
                            <input type="hidden" name="client" value="default_frontend">
                        <input type="hidden" name="proxystylesheet" value="default_frontend">
                    </div>
                </form>
            </div>
            <ul id="global-menu">
                <li><a href="https://uwaterloo.ca/admissions/">Admissions</a></li>
                <li><a href="https://uwaterloo.ca/about/">About Waterloo</a></li>
                <li><a href="https://uwaterloo.ca/faculties-academics/">Faculties &amp; Academics</a></li>
                <li><a href="https://uwaterloo.ca/offices-services/">Offices &amp; Services</a></li>
                <li><a href="http://campaign.uwaterloo.ca/">Support Waterloo</a></li>
            </ul>
        </div>
    </div>
    <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
    <div id="program-search-results">
        <p class="highlight">
            <?php if ($keyword && !count($undergrad_programs) && !count($grad_programs)) { ?>
            Sorry, we couldn't find any related programs.
            <?php }
elseif ($keyword) { ?>
            Here's what we found related to <strong><?php echo $keyword; ?></strong>.
<?php }
else { ?>
            Here's a list of the 280 programs offered at the University of Waterloo.
<?php } ?>
        </p>
        <?php if ($is_pt_or_online) { ?>
        <p>
            Some programs are also available through part-time or online studies.
        </p>
        <?php } ?>
        <?php if (($undergrad || $undergrad == $grad) && (count($undergrad_programs)||count($grad_programs))) { ?>
        <div class="column <?php
                if ($undergrad && !$grad) {
          echo "column-100";
                }
                ?>">
            <div class="results-title">Undergraduate</div>
            <?php for ($i = 0; $i < count($undergrad_programs); $i++) { ?>
            <div class="result">
                <a href="<?php echo $undergrad_programs[$i]['url'] ?>"><?php echo $undergrad_programs[$i]['name'] ?></a>
                <div class="tag-container">
                    <?php if ($undergrad_programs[$i]['part_time']) { ?>
                    <a class="tag" href="http://pts.uwaterloo.ca/about_pts.html" >Part-Time</a>
                    <?php
                    }
if ($undergrad_programs[$i]['online']) : ?>
                    <a class="tag" href="http://de.uwaterloo.ca/how_de_works.html">Online</a>
<?php endif; ?>
                </div>
            </div>
            <?php } ?>
            <?php if (!count($undergrad_programs)) : ?>
            <div class="result">
              <p>Sorry, no undergraduate programs found.</p>
            </div>
            <?php endif; ?>
        </div>
        <?php } ?>
        <?php if (($grad || $undergrad == $grad) && (count($undergrad_programs)||count($grad_programs))) { ?>
        <div class="column <?php
        if ($grad && !$undergrad) {
          echo "column-100";
        } ?>">
            <div class="results-title">Graduate</div>
            <?php for ($i = 0; $i < count($grad_programs); $i++) { ?>
            <div class="result">
                <a href="<?php echo $grad_programs[$i]['url'] ?>"><?php echo $grad_programs[$i]['name'] ?></a>
                <div class="tag-container">
                    <?php if ($grad_programs[$i]['part_time']) { ?>
                    <a class="tag" href="http://pts.uwaterloo.ca/about_pts.html">Part-Time</a>
                    <?php
                    }
if ($grad_programs[$i]['online']) {
                                        ?>
                    <a class="tag" href="http://de.uwaterloo.ca/how_de_works.html">Online</a>
<?php } ?>
                </div>
            </div>
            <?php } ?>
            <?php if (!count($grad_programs)) { ?>
            <div class="result">
                <p>Sorry, no graduate programs found.</p>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
    <div id="program-search-2">
        <div id="program-content-2" class="content-wrapper">
            <div id="program-search-heading" class="col-40">
            <?php if ($keyword && !count($undergrad_programs) && !count($grad_programs)) { ?>
                <h2>Try searching for something else:</h2>
            <?php }
else { ?>
                <h2>Looking for something else?</h2>
<?php } ?>
            </div>
            <div class="col-60">
                <form action="results.php" method="post">
                    <div class="col-100">
                        <input type="text" name="program-keyword" id="program-search-textbox" type="text" placeholder="ENTER A SUBJECT OR AREA OF INTEREST"><!--
                        --><input id="program-search-submitbutton" type="submit" value="FIND MY PROGRAM">
                    </div>
                    <div id="search-checkbox-container" class="col-100 left-justify">
                        <label for="undergraduate-collection"><input type="checkbox" name="undergrad" id="undergraduate-collection" value="1"> Undergraduate</label>
                        <label for="graduate-collection"><input type="checkbox" name="grad" id="graduate-collection" value="1"> Graduate</label>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="quick-link-menu">
        <div id="quick-link-content">
            <h2>Quicklinks</h2>
            <div class="quick-link-group">
                <div class="quick-link">
                    <img src="images/connect-with-us.png" width="131" height="130" alt="Checkmark" />
                    <h3>Connect with us</h3>
                    <p><a href="https://uwaterloo.ca/find-out-more/contact-us">Undergraduate</a></p>
                    <p><a href="https://uwaterloo.ca/discover-graduate-studies/contact-us">Graduate</a></p>
                </div>
                <div class="quick-link">
                    <img src="images/requirements.png" width="131" height="130" alt="List" />
                    <h3>Requirements</h3>
                    <p><a href="https://uwaterloo.ca/find-out-more/admissions/admission-requirements">Undergraduate</a></p>
                    <p><a href="https://uwaterloo.ca/discover-graduate-studies/ready-apply/admission-requirements">Graduate</a></p>
                </div>
                <div class="quick-link">
                    <img src="images/how-to-apply.png" width="131" height="130" alt="FAQ" />
                    <h3>How to apply</h3>
                    <p><a href="https://uwaterloo.ca/find-out-more/admissions/applying-waterloo">Undergraduate</a></p>
                    <p><a href="https://uwaterloo.ca/discover-graduate-studies/ready-apply">Graduate</a></p>
                </div>
                <div class="clearFloat"></div>
            </div>
        </div>
    </div>
    <div id="footer">
      <div id="footer-content">
        <div id="university-address">
          200 University Avenue West<br />
          Waterloo, ON, Canada N2L 3G1<br />
          +1 519 888 4567
        </div>
        <div id="footer-menu">
          <div class="column-3">
            <a class="menu-item" href="https://uwaterloo.ca/about/how-find-us/contact-waterloo">Contact Waterloo</a>
            <a class="menu-item" href="https://uwaterloo.ca/map/">Maps &amp; Directions</a>
            <a class="menu-item" href="http://hr.uwaterloo.ca/mycareer/">Employment</a>
          </div>
          <div class="column-3">
            <a class="menu-item" href="https://uwaterloo.ca/news/">Media</a>
            <a class="menu-item" href="http://studentservices.uwaterloo.ca/disabilities/">Accessibility</a>
            <a class="menu-item" href="https://uwaterloo.ca/about/how-find-us/contact-waterloo/contact-form">Feedback</a>
          </div>
          <div class="column-3">
            <a class="menu-item" href="https://uwaterloo.ca/privacy/">Privacy</a>
            <a class="menu-item" href="https://uwaterloo.ca/copyright">Copyright</a>
          </div>
        </div>
        <div id="social-icons">
          <img src="images/social-media-directory.png" width="111" height="53" alt="social media at Waterloo">
        </div>
      </div>
    </div>
</body>
</html>

<script type="text/javascript">
var search_keyword = "<?php echo $keyword; ?>";
var search_undergrad = "<?php echo ($undergrad || $grad == $undergrad); ?>";
var search_grad = "<?php echo ($grad || $grad == $undergrad); ?>";
</script>
