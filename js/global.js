/**
 * @file
 */

var request, user_ip, user_city, user_region, user_country, search_id;

$(document).ready(function () {
    $.get("http://ipinfo.io", function (response) {
        user_ip = response.ip;
        user_city = response.city;
        user_region = response.region;
        user_country = response.country;
    }, "jsonp")
    .always(function () {
        if (request) {
            request.abort(); }

        request = $.ajax({
            url: "handlers/analytics_search_keywords.php",
            type: "post",
            data: {
                keyword: search_keyword,
                grad: search_grad,
                undergrad: search_undergrad,
                ip: user_ip,
                city: user_city,
                region: user_region,
                country: user_country
            }
        });

        request.done(function (response, textStatus, jqXHR) {
            search_id = response;
        });
    });

    $(".result a").click(function () {
        if (search_id != null) {
            if (request) {
                request.abort(); }

            request = $.ajax({
                url: "handlers/analytics_search_tracker.php",
                type: "post",
                data: {
                    search_id: search_id,
                    program_name: $(this).html(),
                    program_url: $(this).attr("href"),
                },
            });
        }
    });
});
