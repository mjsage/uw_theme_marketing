/**
 * @file
 */

(function ($) {
   $(window).load(function isIe() {
        var ua = window.navigator.userAgent;
        var oldIe = ua.indexOf('MSIE ');
        var newIe = ua.indexOf('Trident/');
        if ((oldIe > -1) || (newIe > -1)) {
            $('html').addClass('isie-old');
        }
    });

    $(document).ready(function () {

      // ****** COPY BLOCK ******.
      $('.field-name-field-sph-copy > .expandable > h2:first-child').click(function () {
        $expandable = $(this).parent();
        if ($expandable.hasClass('expanded')) {
          $expandable.removeClass('expanded');
          $('.expandable-content',$expandable).prev().addClass('last-visible');
        }
        else {
          $expandable.addClass('expanded');
          $('.expandable-content',$expandable).prev().removeClass('last-visible');
        }
      }).wrapInner('<button>');

      // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins
      // .node-type-uw-web-page.
      $('.field-name-field-sph-copy > .expandable > .expandable-content').prev().addClass('last-visible');

      // .node-type-uw-web-page.
      $('.field-name-field-sph-copy > .expandable > .expandable-content :last-child').addClass('last-visible');

      // If there is more than one expandable region on the page, add expand/collapse all functions.
      if ($('.field-name-field-sph-copy > .expandable').length > 1) {
        // .node-type-uw-web-page.
        $('.field-name-field-sph-copy > .expandable:first').before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');

        $('#content .expandable-controls .expand-all').click(function () {
          // Rather than recreate clicking logic here, just click the affected items.
          $('#content .expandable:not(.expanded) h2:first-child').click();
        });

        $('#content .expandable-controls .collapse-all').click(function () {
          // Rather than recreate clicking logic here, just click the affected items.
          $('#content .expandable.expanded h2:first-child').click();
        });
      }

       // ****** BODY SINGLE PAGE ******.
      $('.field-name-body > .expandable > h2:first-child').click(function () {
        $expandable = $(this).parent();
        if ($expandable.hasClass('expanded')) {
          $expandable.removeClass('expanded');
          $('.expandable-content',$expandable).prev().addClass('last-visible');
        }
        else {
          $expandable.addClass('expanded');
          $('.expandable-content',$expandable).prev().removeClass('last-visible');
        }
      }).wrapInner('<button>');

      // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins
      // .node-type-uw-web-page.
      $('.field-name-body > .expandable > .expandable-content').prev().addClass('last-visible');

      // .node-type-uw-web-page.
      $('.field-name-body > .expandable > .expandable-content :last-child').addClass('last-visible');

      // If there is more than one expandable region on the page, add expand/collapse all functions.
      if ($('.field-name-body > .expandable').length > 1) {
        // .node-type-uw-web-page.
        $('.field-name-body > .expandable:first').before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');

        $('#content .expandable-controls .expand-all').click(function () {
          // Rather than recreate clicking logic here, just click the affected items.
          $('#content .expandable:not(.expanded) h2:first-child').click();
        });

        $('#content .expandable-controls .collapse-all').click(function () {
          // Rather than recreate clicking logic here, just click the affected items.
          $('#content .expandable.expanded h2:first-child').click();
        });
      }

      // Do things when the anchor changes.
      window.onhashchange = uw_fdsu_anchors;

      // Trigger the event on page load in case there is already an anchor.
      uw_fdsu_anchors();

      // Function to do things when the anchor changes.
      function uw_fdsu_anchors() {
        if (location.hash) {
          // Check if there is an ID with this name.
          if ($(location.hash).length) {
            // If it's in an unexpanded expandable content area, expand the expandable content area.
            $(location.hash,'.expandable:not(.expanded)').closest('.expandable').find('h2:first-child').click();
            // Scroll to the ID, taking into account the toolbar if it exists
            // "html" works in Firefox, "body" works in Chrome/Safari.
            $('html, body').scrollTop($(location.hash).offset().top - $('#toolbar').height());
          }
        }
      }
    });

})(jQuery);
