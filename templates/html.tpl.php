<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of one page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
  <title><?php print $head_title; ?></title>
  <meta name="msapplication-navbutton-color" content="#000000" />
  <meta name="msapplication-TileColor" content="#000000"/>
  <meta name="msapplication-square70x70logo" content="/university-of-waterloo-logo-tile-70.png"/>
  <meta name="msapplication-square150x150logo" content="/university-of-waterloo-logo-tile-150.png"/>
  <meta name="msapplication-wide310x150logo" content="/university-of-waterloo-logo-tile-310x150.png"/>
  <meta name="msapplication-square310x310logo" content="/university-of-waterloo-logo-tile-310.png"/>
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-57.png" sizes="57x57">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-72.png" sizes="72x72">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-76.png" sizes="76x76">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-114.png" sizes="114x114">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-120.png" sizes="120x120">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-144.png" sizes="144x144">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-152.png" sizes="152x152">
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php
  // Set up Google Analytics differently depending on what the site has configured.
  if (variable_get('uw_cfg_google_analytics_account') || variable_get('google_analytics_enable') == 1) {
    ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      <?php
      if (!(variable_get('uw_cfg_google_analytics_account')) && variable_get('google_analytics_enable') == 1) {
        ?>
        ga('create', 'UA-51776731-1', 'auto');
        ga('send', 'pageview');
        <?php
      }
      if (variable_get('uw_cfg_google_analytics_account') && variable_get('google_analytics_enable') == 0) {
        ?>
        ga('create', '<?php print variable_get('uw_cfg_google_analytics_account'); ?>', 'auto');
        ga('send', 'pageview');
        <?php
      }
      if (variable_get('uw_cfg_google_analytics_account') && variable_get('google_analytics_enable') == 1) {
        ?>
        ga('create', 'UA-51776731-1', 'auto');
        ga('create', '<?php print variable_get('uw_cfg_google_analytics_account') ?>', 'auto', {'name': 'newTracker'}); // New tracker.
        ga('send', 'pageview');
        ga('newTracker.send', 'pageview'); // Send page view for new tracker.
        <?php
      }
      ?>
    </script>
    <?php
  }
  ?>

</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<!--[if gte IE 9]<style type="text/css">.gradient {filter: none;}</style><![endif]-->
<!--[if lt IE 7]><div id="ie6message">Your version of Internet Explorer web browser is insecure, not supported by Microsoft, and does not work with this web site. Please use one of these links to upgrade to a modern web browser: <a href="http://www.mozilla.org/firefox/">Firefox</a>, <a href="http://www.google.com/chrome">Google Chrome</a>, <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home">Internet Explorer</a>.</div><![endif]-->
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
